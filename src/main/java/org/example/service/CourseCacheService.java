package org.example.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.example.model.SdaCourse;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class CourseCacheService {
    private static Long currentId = 0L;
    private static List<SdaCourse> courses = new ArrayList<>();

//            List.of(
//            SdaCourse.builder()
//                    .id(1L)
//                    .name("First course")
//                    .description("First course description")
//                    .price(100)
//                    .build(),
//            SdaCourse.builder()
//                    .id(2L)
//                    .name("Second course")
//                    .description("Second course description")
//                    .price(99)
//                    .build()
//    );

    private static final LoadingCache<Long, SdaCourse> sdaCoursesCache =
            CacheBuilder.newBuilder().maximumSize(10).expireAfterWrite(10, TimeUnit.MINUTES)
                .removalListener(remove -> System.out.println(remove.getCause()))
                .build(new CacheLoader<>() {
                           @Override
                           public SdaCourse load(Long key) throws Exception {
                               System.out.println("Loading object " + key);
                               return courses.stream().filter(course -> course.getId().equals(key))
                                       .findFirst().orElse(null);
                           }
                       }

                );

    private void setCourse(SdaCourse newCourse, SdaCourse course) {
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        sdaCoursesCache.put(newCourse.getId(), newCourse);
        courses.add(newCourse);
    }

    public SdaCourse add(SdaCourse course) {
        SdaCourse newCourse = new SdaCourse();
        newCourse.setId(++currentId);
        setCourse(newCourse, course);
        return newCourse;
    }

    public SdaCourse getById(Long id) {
        try {
            return sdaCoursesCache.get(id);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public SdaCourse update(Long id, SdaCourse course) {
        SdaCourse existingCourse = getById(id);
        setCourse(existingCourse, course);
        return existingCourse;
    }

    public void remove(Long id) {
        courses.stream().filter(c -> c.getId().equals(id)).findFirst()
                .ifPresent(courses::remove);
        sdaCoursesCache.invalidate(id);
    }

    public List<SdaCourse> getAllCourses() {
        sdaCoursesCache.size();
        return new ArrayList<>(sdaCoursesCache.asMap().values());
    }
}
