package org.example.converters;

import org.example.model.SdaCourse;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class HtmlConverter implements HttpMessageConverter<SdaCourse> {
    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return false;
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return clazz.equals(SdaCourse.class) && mediaType.equals(MediaType.TEXT_HTML);
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return List.of(MediaType.TEXT_HTML);
    }

    @Override
    public SdaCourse read(Class<? extends SdaCourse> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return null;
    }

    @Override
    public void write(SdaCourse sdaCourse, MediaType contentType, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        String html = "<p>" + sdaCourse.getId() + "</p><p>" + sdaCourse.getName() + "</p>";
        outputMessage.getBody().write(html.getBytes());
    }
}
