package org.example.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class LongDeserializer extends JsonDeserializer<Long> {
    @Override
    public Long deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String value = ((JsonNode)parser.readValueAsTree()).asText();
        if (value == null || value.trim().equals("")) {
            return null;
        }
        return Long.parseLong(value);
    }
}
