package org.example.annotations;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootApplication
@Animal
public @interface AdvancedAnnotation {

    @AliasFor(annotation = SpringBootApplication.class, attribute = "exclude")
    Class<?>[] excludeClasses() default {};

    @AliasFor(annotation = SpringBootApplication.class, attribute = "scanBasePackages")
    String[] scanBasePackagesAdvanced() default {};

    @AliasFor(annotation = Animal.class, attribute = "isWild")
    boolean isWildAnimal() default true;

    @AliasFor(annotation = SpringBootApplication.class, attribute = "proxyBeanMethods")
    boolean proxyBeans() default true;
}
