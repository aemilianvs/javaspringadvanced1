package org.example.exceptions;

import org.example.model.GenericErrorResponse;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class GenericExceptionController extends AbstractErrorController {

    public GenericExceptionController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping(path = "/error", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<Object> handleError(HttpServletRequest request) {
        Map<String, Object> errorAttributes = getErrorAttributes(request, false);

        int status = (int)errorAttributes.get("status");
        GenericErrorResponse response = GenericErrorResponse.builder()
                .errorMessage((String)errorAttributes.get("message"))
                .build();

        return ResponseEntity.status(status).body(response);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
