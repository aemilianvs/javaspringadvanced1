package org.example.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.serializers.LongDeserializer;
import org.example.serializers.LongSerializer;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SdaCourse {

//    @JsonSerialize(using = LongSerializer.class)
//    @JsonDeserialize(using = LongDeserializer.class)
    private Long id;

    private String name;
    private String description;
    private Integer price;
}
