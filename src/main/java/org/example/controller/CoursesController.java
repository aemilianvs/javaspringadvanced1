package org.example.controller;

import org.example.model.SdaCourse;
import org.example.service.CourseCacheService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/courses", produces = {MediaType.APPLICATION_JSON_VALUE})
public class CoursesController {

    private CourseCacheService coursesService;

    public CoursesController(CourseCacheService coursesService) {
        this.coursesService = coursesService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<SdaCourse> get() throws Exception {
        return coursesService.getAllCourses();
    }

    @RequestMapping(method = RequestMethod.POST)
    public SdaCourse add(@RequestBody SdaCourse course) {
        return coursesService.add(course);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public SdaCourse get(@PathVariable Long id) {
        return coursesService.getById(id);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public SdaCourse update(final @RequestBody SdaCourse course, final @PathVariable Long id) {
        return coursesService.update(id, course);
    }
}
