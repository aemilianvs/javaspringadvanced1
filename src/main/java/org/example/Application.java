package org.example;

import org.example.annotations.AdvancedAnnotation;
import org.example.annotations.Animal;
import org.example.model.Dog;
import org.example.model.SdaCourse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@AdvancedAnnotation(excludeClasses = DataSourceAutoConfiguration.class, isWildAnimal = false, proxyBeans = false)
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Animal(isWild = false)
    private Dog someDog;

    @Animal(isWild = true)
    public Dog getMyDog() {
        return new Dog();
    }
}
